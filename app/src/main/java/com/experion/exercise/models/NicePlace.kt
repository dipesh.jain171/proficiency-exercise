package com.experion.exercise.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NicePlace(
    var title: String?,
    var description: String?,
    var imageHref: String?
) : Parcelable