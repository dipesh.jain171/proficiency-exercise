package com.experion.exercise.models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.experion.exercise.data.repositories.NicePlaceRepository
import com.experion.exercise.viewmodels.NicePlaceModelView

@Suppress("UNCHECKED_CAST")
class NicePlaceViewModelFactory(
    private val repository: NicePlaceRepository
):ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NicePlaceModelView(repository) as  T
    }
}