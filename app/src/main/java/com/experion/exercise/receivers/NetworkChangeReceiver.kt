package com.experion.exercise.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.experion.exercise.data.network.NetworkConnectionInterceptor

class NetworkChangeReceiver : BroadcastReceiver() {
    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {
        var connectivityReceiverListener: ConnectivityReceiverListener? = null
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val netAvailbale = NetworkConnectionInterceptor(context!!).isInternetAvailable()
        connectivityReceiverListener!!.onNetworkConnectionChanged(netAvailbale)

    }
}