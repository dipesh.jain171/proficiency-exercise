package com.experion.exercise.data.network

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface MyApi {

    @GET("facts")
    suspend fun getNicePlace(): Response<NicePlaceResponse>

    companion object {

        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {

            val okkHttpClient=OkHttpClient.Builder()
            .addInterceptor(networkConnectionInterceptor).build()
            return Retrofit.Builder()
                .client(okkHttpClient)
                .baseUrl("https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/")
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(MyApi::class.java)
        }
    }

}