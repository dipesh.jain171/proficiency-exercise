package com.experion.exercise.data.network

import com.experion.exercise.models.NicePlace

data class NicePlaceResponse(
    val title: String?,
    var rows: List<NicePlace>?
)