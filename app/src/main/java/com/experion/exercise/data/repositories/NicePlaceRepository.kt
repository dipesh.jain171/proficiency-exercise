package com.experion.exercise.data.repositories

import com.experion.exercise.data.network.MyApi
import com.experion.exercise.data.network.NicePlaceResponse
import com.experion.exercise.data.network.SafeApiRequest

class NicePlaceRepository(private val myApi: MyApi) : SafeApiRequest() {
    suspend fun getNicePlaces(): NicePlaceResponse {
        return apiRequest {
           myApi.getNicePlace()
        }
    }
}