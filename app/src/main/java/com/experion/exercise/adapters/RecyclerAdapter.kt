package com.experion.exercise.adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.experion.exercise.R
import com.experion.exercise.models.NicePlace

class RecyclerAdapter(private val mContext: Context) :
    RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(mContext).inflate(R.layout.layout_nice_place_item, parent, false)
        )
    }

    private var nicePlaces = ArrayList<NicePlace>()

    override fun getItemCount() = nicePlaces.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        setDetails(holder, position)
    }

    private fun setDetails(holder: RecyclerAdapter.ViewHolder, position: Int) {
        var nicePlace = nicePlaces[position]
        if (nicePlace.imageHref != null && !nicePlace.imageHref.toString().isEmpty()) {
            //Also if we don't want show imageView if failed to load image than we can handle using listener
            //In this can im not hiding image view
            val requestOptions = RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop()
            Glide.with(mContext)
                .load(nicePlace.imageHref)
                .apply(requestOptions)
                .error(R.drawable.ic_baseline_image_24)
                .into(holder.imImage)
            holder.imImage.visibility = View.VISIBLE
        } else {
            holder.imImage.visibility = View.GONE
        }
        if (nicePlace.title != null && !nicePlace.title.toString().isEmpty()) {
            holder.tvTitle.text = nicePlace.title
            holder.tvTitle.visibility = View.VISIBLE
        } else {
            holder.tvTitle.visibility = View.GONE
        }
        if (nicePlace.description != null && !nicePlace.description.toString().isEmpty()) {
            holder.tvSubtitle.text = nicePlace.description
            holder.tvSubtitle.visibility = View.VISIBLE
        } else {
            holder.tvSubtitle.visibility = View.GONE
        }


    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTitle = itemView.findViewById<AppCompatTextView>(R.id.tv_title)
        val tvSubtitle = itemView.findViewById<AppCompatTextView>(R.id.tv_subtitle)
        val imImage = itemView.findViewById<AppCompatImageView>(R.id.im_image)
    }

    fun setData(newList: List<NicePlace>) {
        nicePlaces.clear()
        nicePlaces.addAll(newList)
        /*Here is have remove null value from List because empty item view displaying*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            nicePlaces.removeIf { obj: NicePlace -> obj.title == null && obj.description == null }
        } else {
            val itr = nicePlaces.iterator()
            while (itr.hasNext()) {
                val curr: NicePlace? = itr.next()
                if (curr?.title == null && curr?.description == null) {
                    itr.remove() // remove nulls
                }
            }
        }
        notifyDataSetChanged()
    }
}

