package com.experion.exercise.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.experion.exercise.data.network.NicePlaceResponse
import com.experion.exercise.data.repositories.NicePlaceRepository
import com.experion.exercise.ui.NicePlaceListener
import com.experion.exercise.util.ApiException
import com.experion.exercise.util.Coroutines
import com.experion.exercise.util.NoInternetException


class NicePlaceModelView(
    private val repository: NicePlaceRepository
) : ViewModel() {
    var nicePlaceListener: NicePlaceListener? = null
    private var mutableLiveData: MutableLiveData<NicePlaceResponse>? =
        MutableLiveData<NicePlaceResponse>()

    fun getNicePlaceMutableLiveData(): MutableLiveData<NicePlaceResponse>? {
        return mutableLiveData
    }

    fun getNicePlaces(forceCheck: Boolean) {
        nicePlaceListener?.onStarted()
        Coroutines.main {
            try {
                val response = repository.getNicePlaces()
                response.rows?.let {
                    mutableLiveData?.value=response
                    //nicePlaceListener?.onSuccess(response.title, it)
                    return@main
                }
                nicePlaceListener?.onFailed("No recoded found!")
            } catch (e: ApiException) {
                nicePlaceListener?.onFailed(e.message!!)
            } catch (e: NoInternetException) {
                nicePlaceListener?.onFailed(e.message!!)
            }
        }


    }


}