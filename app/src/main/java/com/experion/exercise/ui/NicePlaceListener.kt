package com.experion.exercise.ui

import com.experion.exercise.models.NicePlace

interface NicePlaceListener {
    fun onStarted()

    fun onFailed(message: String)
}