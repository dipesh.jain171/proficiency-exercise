package com.experion.exercise.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.experion.exercise.R
import com.experion.exercise.adapters.RecyclerAdapter
import com.experion.exercise.data.network.MyApi
import com.experion.exercise.data.network.NetworkConnectionInterceptor
import com.experion.exercise.data.network.NicePlaceResponse
import com.experion.exercise.data.repositories.NicePlaceRepository
import com.experion.exercise.models.NicePlaceViewModelFactory
import com.experion.exercise.receivers.NetworkChangeReceiver
import com.experion.exercise.viewmodels.NicePlaceModelView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_nice_place.*
import kotlinx.android.synthetic.main.layout_progress.*

class NicePlaceFragment : Fragment(), NicePlaceListener,
    NetworkChangeReceiver.ConnectivityReceiverListener {
    private var snackBar: Snackbar? = null
    private lateinit var recyclerAdapter: RecyclerAdapter
    var mContext: Context? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_nice_place, container, false)

    var viewModel: NicePlaceModelView? = null
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*This can also achieve using Kodein DI on Android https://kodein.org/Kodein-DI/?5.0/android but for the demo app im not creating  */
        val networkConnectionInterceptor = NetworkConnectionInterceptor(mContext!!)
        val myApi = MyApi(networkConnectionInterceptor)
        val repository = NicePlaceRepository(myApi)
        val factory = NicePlaceViewModelFactory(repository)
        viewModel = ViewModelProvider(this, factory).get(NicePlaceModelView::class.java)
        viewModel!!.nicePlaceListener = this
        recyclerAdapter = RecyclerAdapter(mContext!!)
        val mLayoutManager = LinearLayoutManager(mContext)
        rv_nice_place.layoutManager = mLayoutManager
        rv_nice_place.itemAnimator = DefaultItemAnimator()
        recyclerAdapter.setData(arrayListOf())
        rv_nice_place.adapter = recyclerAdapter
        viewModel!!.getNicePlaces(true)
        viewModel?.getNicePlaceMutableLiveData()?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                layout_progress.visibility = View.GONE
                setTitle(it)
                if (it.rows != null) {
                    recyclerAdapter.setData(it.rows!!)
                }
                setViewForNoRecord()
            } else {
                setViewForNoRecord()
            }
        })
    }

    private fun setTitle(it: NicePlaceResponse) {
        val aa = activity as AppCompatActivity?
        aa?.supportActionBar?.title = it.title
    }

    fun onMenuRefreshClick() {
        viewModel!!.getNicePlaces(true)
    }

    private fun setViewForNoRecord() {
        if (recyclerAdapter.itemCount > 0) {
            tv_no_content?.visibility = View.GONE
        } else {
            tv_no_content?.visibility = View.VISIBLE
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    override fun onResume() {
        super.onResume()
        NetworkChangeReceiver.connectivityReceiverListener = this
    }

    private fun showNetworkMessage(isConnected: Boolean) {
        if (!isConnected) {
            snackBar = Snackbar.make(
                rootLayout,
                "You are offline",
                Snackbar.LENGTH_LONG
            )
            snackBar?.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snackBar?.show()
        } else {
            snackBar?.dismiss()
        }
    }

    override fun onStarted() {
        layout_progress.visibility = View.VISIBLE
    }


    override fun onFailed(message: String) {
        layout_progress.visibility = View.GONE
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show()
        setViewForNoRecord()
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        showNetworkMessage(isConnected)
    }

}